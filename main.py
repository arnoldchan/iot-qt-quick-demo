import sys

from time import strftime, localtime
from gpiozero import LED

from PyQt5.QtGui import QGuiApplication
from PyQt5.QtQml import QQmlApplicationEngine
from PyQt5.QtCore import QTimer, QObject, pyqtSignal, pyqtSlot

app = QGuiApplication(sys.argv)

engine = QQmlApplicationEngine()
engine.quit.connect(app.quit)
engine.load('view.qml')

class Backend(QObject):
    def __init__(self):
        super().__init__()
        self.red = LED(22)
        self.amber = LED(27)
        self.green = LED(17)

    @pyqtSlot(str, int)
    def toggle_led(self, led_colour, position):
        led = eval(f'self.{led_colour}')

        if bool(position):
            led.on()
        else:
            led.off()

# Define our backend object, which we pass to QML.
backend = Backend()
engine.rootObjects()[0].setProperty('backend', backend)
sys.exit(app.exec())