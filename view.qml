import QtQuick 2.4
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.11

ApplicationWindow {  
    width: 640
    height: 410
    visible: true
    title: "IoT Qt Quick Demo"

    // property bool red: false
    // property bool amber: false
    // property bool green: false
    property QtObject backend

    RowLayout {
        spacing: 20
        anchors.fill: parent

        Switch {
            text: "Red LED"
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            onToggled: {
                backend.toggle_led('red', position)
            }
        }
        Switch {
            text: "Amber LED"
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            onToggled: {
                backend.toggle_led('amber', position)
            }
        }
        Switch {
            text: "Green LED"
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            onToggled: {
                backend.toggle_led('green', position)
            }
        }
        Text {
            text: curr_time
        }
    }
}